from .event import Event2,Event3

class AttackEvent(Event2):
    NAME = "attack"
    def perform(self):
        if not self.object.has_prop("attackable"):
            self.fail()
            return self.inform("attack.failed")
        else:
            self.actor.take_damage(self.object.get_dmg())
            self.object.take_damage()
            return self.inform("attack")


class AttackWithEvent(Event3):
    NAME = "attack-with"
    def perform(self):
        if not self.object2.has_prop("weapon"):
            self.fail()
            return self.inform("attack-with.failed")
        else:
            self.object.take_damage(self.object2)
            self.inform("attack-with")
