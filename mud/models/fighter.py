from .model import Model
from .mixins.located import Located


class Fighter(Located, Model):
    """a Fighter is located in the world."""

    # --------------------------------------------------------------------------
    # initialization
    # --------------------------------------------------------------------------

    def __init__(self, **kargs):
        super().__init__(**kargs)

    # --------------------------------------------------------------------------
    # initialization from YAML data
    # --------------------------------------------------------------------------

    def init_from_yaml(self, data, world):
        super().init_from_yaml(data, world)

    def update_from_yaml(self, data, world):
        super().update_from_yaml(data, world)

    # --------------------------------------------------------------------------
    # API for saving the dynamic part of objects to YAML (via JSON)
    # --------------------------------------------------------------------------

    def archive_into(self, obj):
        super().archive_into(obj)

    def get_hp(self):
        for prop in self.get_props():
            if prop[:3]=="hp=":
                return prop[3:]
        return 0

    def get_def(self):
        for prop in self.get_props():
            if prop[:4]=="def=":
                return prop[4:]
        return 0

    def set_hp(self,hp):
        for prop in self.get_props():
            if prop[:3]=="hp=":
                self.remove_prop(prop)
        self.add_prop("hp="+str(hp))

    def take_damage(self,weapon=None):
        if weapon is None:
            damage=1
        else:
            damage=int(weapon.get_dmg())
        current_hp=int(self.get_hp())
        if current_hp-damage <= 0:
            self.set_hp(0)
            self.kill()
        else:
            self.set_hp(current_hp-damage)

    def kill(self):
        self.move_to(None)

    def _has_prop(self, prop, context=None):
        for prop2 in self.get_props():
            if prop==prop2:
                return True
        return False

    def get_dmg(self):
        for prop in self.get_props():
            if prop[:4]=="dmg=":
                return prop[4:]
        return 0